import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import { TrainingsComponent } from './trainings/trainings.component';
import { ExercicesComponent } from './exercices/exercices.component';
import { TrainingsStartComponent } from './trainings/trainings-start/trainings-start.component';
import { TrainingDetailComponent } from './trainings/training-detail/training-detail.component';
import { TrainingEditComponent } from './trainings/training-edit/training-edit.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'trainings', component: TrainingsComponent , canActivate: [AuthGuard], children: [
    { path: '', component: TrainingsStartComponent},
    { path: 'new', component: TrainingEditComponent},
    { path: ':id', component: TrainingDetailComponent},
    { path: ':id/edit', component: TrainingEditComponent}
  ]},
  { path: 'exercices', component: ExercicesComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

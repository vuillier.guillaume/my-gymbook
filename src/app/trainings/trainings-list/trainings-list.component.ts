import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { TrainingsService } from '../trainings.service';
import { Training } from '../training.model';

@Component({
  selector: 'app-trainings-list',
  templateUrl: './trainings-list.component.html',
  styleUrls: ['./trainings-list.component.scss']
})
export class TrainingsListComponent implements OnInit, OnDestroy {
  trainings: Training[];
  subscription: Subscription;

  constructor(private trainingService: TrainingsService) { }

  ngOnInit() {
    this.subscription = this.trainingService.trainingsChanged.subscribe(
      (trainings: Training[]) => {
        this.trainings = trainings;
      }
    );
    this.trainings = this.trainingService.getTrainings();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

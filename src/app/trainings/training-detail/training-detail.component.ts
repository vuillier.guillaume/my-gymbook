import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Training } from '../training.model';
import { TrainingsService } from '../trainings.service';

@Component({
  selector: 'app-training-detail',
  templateUrl: './training-detail.component.html',
  styleUrls: ['./training-detail.component.scss']
})
export class TrainingDetailComponent implements OnInit {
  id: number;
  training: Training;

  constructor(private trainingService: TrainingsService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.id = +params.id;
        this.training = this.trainingService.getTraining(this.id);
      }
    );
  }

  onDeleteTraining() {
    this.trainingService.deleteTraining(this.id);
    this.router.navigate(['trainings']);
  }

}

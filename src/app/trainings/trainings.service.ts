import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Training } from './training.model';
import { Exercice } from '../exercices/exercice.model';

@Injectable({
  providedIn: 'root'
})
export class TrainingsService {
  trainingsChanged = new Subject<Training[]>();

  private trainings: Training[] = [
    new Training(
      'Pectoraux',
      'Un entraînement ciblé sur les muscles pectoraux',
      [
        new Exercice('Développé couché', 'Pectoraux'),
        new Exercice('Développé haltères', 'Pectoraux'),
        new Exercice('Pompes', 'Pectoraux'),
      ]
    ),
    new Training(
      'Dos',
      'Pour avoir un dos plus large que l\'ouverture d\'une porte',
      [
        new Exercice('Tirage horizontal', 'Grand dorsal'),
        new Exercice('Tractions', 'Grand dorsal'),
        new Exercice('Tirage vertical', 'Grand dorsal'),
      ]
    )
  ];

  constructor() { }

  getTrainings() {
    return this.trainings.slice();
  }

  getTraining(id: number) {
    return this.trainings[id];
  }

  addTraining(training: Training) {
    this.trainings.push(training);
    this.trainingsChanged.next(this.trainings.slice());
  }

  updateTraining(index: number, newTraining: Training) {
    this.trainings[index] = newTraining;
    this.trainingsChanged.next(this.trainings.slice());
  }

  deleteTraining(index: number) {
    this.trainings.splice(index, 1);
    this.trainingsChanged.next(this.trainings.slice());
  }
}

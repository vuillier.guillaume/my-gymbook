import { Exercice } from '../exercices/exercice.model';

export class Training {
    constructor(public name: string, public description: string, public exercices: Exercice[]) {}
}

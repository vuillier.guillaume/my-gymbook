import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { TrainingsService } from '../trainings.service';


@Component({
  selector: 'app-training-edit',
  templateUrl: './training-edit.component.html',
  styleUrls: ['./training-edit.component.scss']
})
export class TrainingEditComponent implements OnInit {
  id: number;
  editMode = false;
  trainingForm: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private trainingsService: TrainingsService,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      (params: Params) => {
        this.id = params.id;
        this.editMode = params.id != null;
        this.initForm();
      }
    );
  }

  private initForm() {
    let trainingName = '';
    let trainingDesc = '';
    const trainingExercices = new FormArray([]);

    if (this.editMode) {
      const training = this.trainingsService.getTraining(this.id);
      trainingName = training.name;
      trainingDesc = training.description;
      if (training.exercices) {
        for (const exercice of training.exercices) {
          trainingExercices.push(
            new FormGroup({
              name: new FormControl(exercice.name, Validators.required)
            })
          );
        }
      }
    }

    this.trainingForm = new FormGroup({
      name: new FormControl(trainingName, Validators.required),
      description: new FormControl(trainingDesc, Validators.required),
      exercices: trainingExercices
    });
  }

  getControls() {
    return (this.trainingForm.get('exercices') as FormArray).controls;
  }

  onAddExercice() {
    (this.trainingForm.get('exercices') as FormArray).push(
      new FormGroup({
        name: new FormControl(null, Validators.required)
      })
    );
  }

  onDeleteExercice(index: number) {
    (this.trainingForm.get('exercices') as FormArray).removeAt(index);
  }

  onCancel() {
    this.router.navigate(['../'], {relativeTo: this.activatedRoute});
  }

  onSubmit() {
    if (this.editMode) {
      this.trainingsService.updateTraining(this.id, this.trainingForm.value);
    } else {
      this.trainingsService.addTraining(this.trainingForm.value);
    }
    this.onCancel();
  }

}

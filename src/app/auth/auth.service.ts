import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase/app';
import 'firebase/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string;
  loginErrorMessage: string;

  constructor(private router: Router) { }

  signupUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['/']);
      })
      .catch((error) => console.log(error));
  }

  loginUser(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        () => {
          firebase.auth().currentUser.getIdToken()
            .then(
              (token: string) => this.token = token
            );
          this.router.navigate(['/']);
        }
      )
      .catch((error) => {
        const errorCode = error.code;

        if (errorCode === 'auth/wrong-password') {
          alert('Le mot de passe est incorrect');
        } else if (errorCode === 'auth/user-not-found') {
          alert('L\'adresse mail entrée n\'est associée à aucun compte');
        }
      });
  }

  loadUser() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user === null) {
        this.token = null;
      } else {
        user.getIdToken().then(
          (token: string) => this.token = token
        );
      }
    });
  }

  getToken() {
    firebase.auth().currentUser.getIdToken()
      .then(
        (token: string) => this.token = token
      );

    return this.token;
  }

  isAuthenticated() {
    return this.token != null;
  }

  logout() {
    firebase.auth().signOut();
    this.token = null;
    this.router.navigate(['/login']);
  }

}

import { Injectable } from '@angular/core';

import { Exercice } from './exercice.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExercicesService {

  private exercices: Exercice[] = [
    new Exercice('Développé couché', 'Pectoraux'),
    new Exercice('Développé incliné', 'Pectoraux'),
    new Exercice('Pompes', 'Pectoraux')
  ];

  exercicesChanged = new Subject<Exercice[]>();
  exerciceIsEdited = new Subject<number>();

  constructor() { }

  getExercices() {
    return this.exercices.slice();
  }

  getExercice(index: number) {
    return this.exercices[index];
  }

  addExercice(exercice: Exercice) {
    this.exercices.push(exercice);
    this.exercicesChanged.next(this.exercices.slice());
  }

  updateExercice(index: number, exerciceUpdated: Exercice) {
    this.exercices[index] = exerciceUpdated;
    this.exercicesChanged.next(this.exercices.slice());
  }

  deleteExercice(index: number) {
    this.exercices.splice(index, 1);
    this.exercicesChanged.next(this.exercices.slice());
  }
}

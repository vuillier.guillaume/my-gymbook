import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

import { Exercice } from '../exercice.model';
import { ExercicesService } from '../exercices.service';

@Component({
  selector: 'app-exercice-edit',
  templateUrl: './exercice-edit.component.html',
  styleUrls: ['./exercice-edit.component.scss']
})
export class ExerciceEditComponent implements OnInit, OnDestroy {
  @ViewChild('f') exerciceForm: NgForm;
  subscription: Subscription;
  editMode = false;
  editedExerciceIndex: number;
  editedExercice: Exercice;

  constructor(private exercicesService: ExercicesService) { }

  ngOnInit() {
    this.subscription = this.exercicesService.exerciceIsEdited.subscribe(
      (index: number) => {
        this.editMode = true;
        this.editedExerciceIndex = index;
        this.editedExercice = this.exercicesService.getExercice(index);
        this.exerciceForm.setValue({
          name: this.editedExercice.name,
          muscle: this.editedExercice.muscle
        });
      }
    );
  }

  onSubmit(form: NgForm) {
    const value = form.value;
    const newExercice = new Exercice(value.name, value.muscle);
    if (this.editMode === true) {
      this.exercicesService.updateExercice(this.editedExerciceIndex, newExercice);
    } else {
      this.exercicesService.addExercice(newExercice);
    }
    this.onClear();
  }

  onClear() {
    this.exerciceForm.reset();
    this.editMode = false;
  }

  onDelete() {
    this.exercicesService.deleteExercice(this.editedExerciceIndex);
    this.onClear();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchEx'
})
export class SearchExPipe implements PipeTransform {

  transform(value: any, searchString: string, propName: string): any {
    if (value.length === 0 || searchString === '' || searchString == null) {
      return value;
    }

    return value.filter(e => e[propName].toLowerCase().indexOf(searchString) > -1 );

  }

}

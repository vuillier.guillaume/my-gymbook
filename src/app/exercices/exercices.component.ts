import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { ExercicesService } from './exercices.service';
import { Exercice } from './exercice.model';

@Component({
  selector: 'app-exercices',
  templateUrl: './exercices.component.html',
  styleUrls: ['./exercices.component.scss']
})
export class ExercicesComponent implements OnInit, OnDestroy {
  exercices: Exercice[];
  private subscription: Subscription;
  searchString = '';

  constructor(private exercicesService: ExercicesService) { }

  ngOnInit() {
    this.exercices = this.exercicesService.getExercices();
    this.subscription = this.exercicesService.exercicesChanged.subscribe(
      (exercices: Exercice[]) => {
        this.exercices = exercices;
      }
    );
  }

  onEditExercice(index: number) {
    this.exercicesService.exerciceIsEdited.next(index);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase/app';

import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private authService: AuthService) {}

  ngOnInit() {
    const config = {
      apiKey: 'AIzaSyAu1bxWaPHCSCUVvwKJUSPgV-TXrDfM1Gs',
      authDomain: 'my-gymbook.firebaseapp.com',
      databaseURL: 'https://my-gymbook.firebaseio.com',
      projectId: 'my-gymbook',
      storageBucket: '',
      messagingSenderId: '406527291513'
    };
    firebase.initializeApp(config);

    this.authService.loadUser();
  }
}

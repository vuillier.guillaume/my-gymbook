import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { TrainingsComponent } from './trainings/trainings.component';
import { TrainingsListComponent } from './trainings/trainings-list/trainings-list.component';
import { TrainingDetailComponent } from './trainings/training-detail/training-detail.component';
import { ExercicesComponent } from './exercices/exercices.component';
import { ExerciceEditComponent } from './exercices/exercice-edit/exercice-edit.component';
import { SearchExPipe } from './exercices/search-ex.pipe';
import { TrainingsStartComponent } from './trainings/trainings-start/trainings-start.component';
import { TrainingEditComponent } from './trainings/training-edit/training-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    TrainingsComponent,
    TrainingsListComponent,
    TrainingDetailComponent,
    ExercicesComponent,
    ExerciceEditComponent,
    SearchExPipe,
    TrainingsStartComponent,
    TrainingEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
